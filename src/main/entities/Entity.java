/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Entity
 Sep 16, 2015
 */
package main.entities;

import main.gui.ResourceStore;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

/**
 *
 * @author Beppe
 */
public class Entity
{

    private EEntities type = EEntities.PALM;

    private int scale = 0;
    private Image image;
    
    public void draw(Graphics g, int x, int y, int scale)
    {
        if (this.scale != scale)
        {
            image = ResourceStore.get().getImage(type, scale);
            this.scale = scale;
        }

        g.drawImage(image, x, y);
    }
}
