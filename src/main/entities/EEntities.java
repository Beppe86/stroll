/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 EEntities
 Sep 25, 2015
 */
package main.entities;

import main.gui.IPlaceable;

/**
 *
 * @author Beppe
 */
public enum EEntities implements IPlaceable
{
    PALM("s-tropical1.png");
    
    private String name;
    
    private EEntities(String name)
    {
        this.name = name;
    }
    
    @Override
    public String getFileName()
    {
        return name;
    }
}
