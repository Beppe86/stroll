/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Main
 Sep 21, 2015
 */
package main;

import main.system.Game;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

/**
 *
 * @author Beppe
 */
public class Main
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] argv)
    {
        try
        {
            AppGameContainer container = new AppGameContainer(new Game());
            container.setDisplayMode(800, 600, false);
            container.start();
        } catch (SlickException e)
        {
            e.printStackTrace();
        }
    }
}
