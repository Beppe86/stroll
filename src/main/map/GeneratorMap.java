/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 GeneratorMap
 Sep 16, 2015
 */
package main.map;

import java.awt.Point;
import main.entities.Entity;
import main.system.HexPoint;

/** 
 *
 * @author Beppe
 */
public class GeneratorMap
{
    private Map map;
    private final int borderSize = 4;
    
    public GeneratorMap(int numberOfChars)
    {
        map = new Map();
        if (numberOfChars < 10)
            randomizeMap(10 + borderSize); //Smallest map is size of 10 chars
        else
            randomizeMap(numberOfChars + borderSize);
        
        placeChars();
    }

    private void randomizeMap(int size)
    {
        int q, r, rStart = 0;
        
        for (q = 0; q < size; q++)
        {
            for (r = rStart; r < size + rStart; r++)
            {
                HexPoint p = new HexPoint(q, r);
                map.put(p, randomizeTile());
            }
            
            if (q % 2 == 0)
                rStart = -1 * q / 2;
            else
                rStart = rStart - 1;
        }
    }
    
    private void placeChars()
    {
        int qMiddle = map.getWidth()[1] / 2;
        int rMiddle = qMiddle / 2;
        map.getEntities().put(map.get(new HexPoint(qMiddle, rMiddle)), new Entity());
    }
    
    private Tile randomizeTile()
    {
        int max = ETerrain.values().length - 2;
        int random = (int)Math.round(Math.random() * max);
        return new Tile(ETerrain.values()[random]);
    }
    
    public Map getMap()
    {
        return map;
    }
}
