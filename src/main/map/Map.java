/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Map
 Sep 16, 2015
 */
package main.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import main.entities.EEntities;
import main.entities.Entity;
import main.system.HexPoint;

/**
 *
 * @author Beppe
 */
public class Map extends HashMap<HexPoint, Tile>
{
    private HashMap<Tile, Entity> entities = new HashMap<>();
    
    public void moveEntity(Tile fromTile, Tile toTile)
    {
        Entity entity = entities.get(fromTile);
        entities.remove(fromTile);
        entities.put(toTile, entity);
        
    }
    
    /**
     * Returns a HexPoint[] with all the tiles in the same column as parameter
     * HexPoint this array.length can be used to count how many hexes exists on
     * the X axis
     *
     * @param qLineToCheck this Q line will be return as HexPoint[]
     * @return returns HexPoint[]
     */
    public HexPoint[] getHeightArray(HexPoint qLineToCheck)
    {
        int column = qLineToCheck.q;
        List<HexPoint> list = new ArrayList<>();

        for (HexPoint p : this.keySet())
        {
            if (p.q == column)
            {
                list.add(p);
            }
        }

        Collections.sort(list, new QComparator());
        return list.toArray(new HexPoint[list.size()]);
    }

    public void hexToggleSelected(Tile tile)
    {
        //Tile tile = this.get(p);
        tile.changeClicked();
        //return tile;
    }

    /**
     * Returns the smallest and biggest Q cords in Map. [0] = small, [1] = big
     *
     * @return new int[] {min, max}
     */
    public int[] getWidth()
    {
        int min = 0;
        int max = 0;

        for (HexPoint p : this.keySet())
        {
            if (p.q < min)
            {
                min = p.q;
            } else if (p.q > max)
            {
                max = p.q;
            }
        }
        return new int[]
        {
            min, max
        };
    }
    
    public HashMap<Tile, Entity> getEntities()
    {
        return entities;
    }

    class QComparator implements Comparator<HexPoint>
    {

        @Override
        public int compare(HexPoint a, HexPoint b)
        {
            return a.r < b.r ? -1 : a.r == b.r ? 0 : 1;
        }
    }
}
