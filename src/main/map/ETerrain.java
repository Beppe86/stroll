/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 ETerrain
 Sep 16, 2015
 */
package main.map;

import main.gui.IPlaceable;

/**
 *
 * @author Beppe
 */
public enum ETerrain implements IPlaceable
{
    IMPASSABLE("heavycactus.png"), ROUGH("deadforest.png"), CLEAR("cactus.png"), PAVED("rubbleblack.png"), ERROR("volcanos.png");
    
    private String name;
    
    private ETerrain(String name)
    {
        this.name = name;
    }
    
    @Override
    public String getFileName()
    {
        return name;
    }
}
