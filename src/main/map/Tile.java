/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Tile
 Sep 16, 2015
 */
package main.map;

import org.newdawn.slick.Graphics;
import java.awt.image.BufferedImage;
import main.entities.Entity;
import main.gui.ResourceStore;
import main.system.Game;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;

/**
 *
 * @author Beppe
 */
public class Tile
{

    public static int width = Game.scale * 2;
    public static int height = (int) Math.round(Math.sqrt(3) / 2 * width);

    private ETerrain terrain;
    //private Entity entities;

    //Graphics
    private int scale = 0;
    private Image image;
    private boolean clicked;

    public Tile(ETerrain terrain)
    {
        this.terrain = terrain;
    }

//    public void setEntity(Entity entity)
//    {
//        entities = entity;
//    }
//    
//    public Entity getEntity()
//    {
//        return entities;
//    }
    
    public void changeClicked()
    {
        clicked = !clicked;
    }

    public void draw(Graphics g, int q, int r, int x, int y, int scale)
    {
        if (!clicked)
        {
            if (this.scale != scale)
            {
                image = ResourceStore.get().getImage(terrain, scale);
                this.scale = scale;
            }

            g.drawImage(image, x, y);
            g.setColor(Color.yellow);
            g.drawString(q + ", " + r, x + 25, y + 35);

//            if (entities != null)
//            {
//                entities.draw(g, x, y, scale);
//            }
        }
    }
}
