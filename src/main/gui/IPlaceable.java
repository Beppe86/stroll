/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 IPlaceable
 Sep 25, 2015
 */
package main.gui;

/**
 *
 * @author Beppe
 */
public interface IPlaceable
{
    public String getFileName();
}
