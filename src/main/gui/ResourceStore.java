/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 ResourceStore
 Sep 16, 2015
 */
package main.gui;

import java.net.URL;
import java.util.HashMap;
import main.map.ETerrain;
import main.map.Tile;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 *
 * @author Beppe
 */
public class ResourceStore
{

    private static ResourceStore single = new ResourceStore();
    private int scale = 0;

    public static ResourceStore get()
    {
        return single;
    }

    private HashMap<ETerrain, Image> images = new HashMap();

    public Image getImage(IPlaceable placeable, int scale)
    {
        if (images.get(placeable) != null && this.scale == scale)
        {
            return images.get(placeable);
        }
        
        if (images.get(placeable) != null)
        {
            return images.get(placeable).getScaledCopy((int)Tile.width, (int)Tile.height);
        }

        Image sourceImage = null;

        try
        {
            URL url = this.getClass().getClassLoader().getResource("main/resources/" + placeable.getFileName());

            if (url == null)
            {
                System.out.println("Can't load image");
            }

            return new Image(url.getFile()).getScaledCopy((int)Tile.width, (int)(Math.round(Tile.height)));
            
        } catch (SlickException ex)
        {
            System.out.println("Can't load image");
        }
        
        return null;
    }
}
