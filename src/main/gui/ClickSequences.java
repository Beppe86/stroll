/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 ClickSequences
 Oct 2, 2015
 */
package main.gui;

import main.entities.Entity;
import main.map.Map;
import main.map.Tile;
import main.system.HexPoint;

/**
 *
 * @author Beppe
 */
public class ClickSequences
{

    private final Map map;

    private Tile selectedTile;
    private Entity selectedEntity;

    public ClickSequences(Map map)
    {
        this.map = map;
    }

    public void hexClicked(Tile tile)
    {
        //Same tile is clicked on for the second time (deselect)
        if (selectedTile == tile)
        {
            map.hexToggleSelected(selectedTile);
            selectedTile = null;
        } //a tile is already selected when a different tile is clicked
        else if (selectedTile != null)
        {
            map.hexToggleSelected(selectedTile);
            if (map.getEntities().containsKey(selectedTile))
            {
                map.moveEntity(selectedTile, tile);
            }
            selectedTile = tile;
            map.hexToggleSelected(selectedTile);
        } //nothing is selected
        else
        {
            selectedTile = tile;
            map.hexToggleSelected(selectedTile);
        }

//        if (selectedTile.getEntity() != null)
//        {
//            selectedEntity = selectedTile.getEntity();
//        } else if (selectedEntity != null)
//        {
//            //map.
//        }
    }
}
