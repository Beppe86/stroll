/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Screen
 Sep 22, 2015
 */
package main.gui;

import java.awt.Point;
import main.entities.Entity;
import static main.gui.HelpWorldView.*;
import main.map.Map;
import main.map.ETerrain;
import main.map.Tile;
import main.system.Game;
import main.system.HexPoint;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 *
 * @author Beppe
 */
public class Screen
{

    private int scale = Game.scale;
    private Map map;
    private ClickSequences clicks;

    private HexPoint topLeftOfScreen = new HexPoint(1, 1);
    private float screenOffsetX = 0;
    private float screenOffsetY = 0;
    private int tilesOnScreenX;
    private int tilesOnScreenY;

    public Screen(GameContainer container, Map map, ClickSequences clicks)
    {
        this.map = map;
        this.clicks = clicks;

        tilesOnScreenX = (int) Math.round(container.getWidth() / Tile.width * 1.25 + 2); //added 1.25 q is packed tighter
        tilesOnScreenY = (int) Math.round(container.getHeight() / (Tile.height) + 2); //added -11 because helpWorldView needs it
    }

    public void hexClicked(int x, int y)
    {
        HexPoint p = calcClickToHex(x, y, topLeftOfScreen, screenOffsetX, screenOffsetY);
        clicks.hexClicked(map.get(p));
    }

    public void render(Graphics g, Map map)
    {

        int q, r, rStart = 1;

        for (q = 1; q < tilesOnScreenX + 2; q++) //adds 4 to exapand whole way
        {
            for (r = rStart; r < tilesOnScreenY + 2 + rStart; r++) //same
            {
                HexPoint hexP = new HexPoint(q, r);
                Point pixelP = calcCoordsToPixels(hexP, screenOffsetX, screenOffsetY);

                hexP.setQ(hexP.q + topLeftOfScreen.q);
                hexP.setR(hexP.r + topLeftOfScreen.r);

                Tile tile;

                if (map.containsKey(hexP))
                {
                    tile = map.get(hexP);
                } else
                {
                    tile = new Tile(ETerrain.ERROR);
                }

                tile.draw(g, hexP.q, hexP.r, pixelP.x, pixelP.y, scale);

                Entity entityToDraw = map.getEntities().get(tile);
                if (map.getEntities().containsKey(tile))
                {
                    map.getEntities().get(tile).draw(g, pixelP.x, pixelP.y, scale);
                }
            }

            if (q % 2 == 0)
            {
                rStart = -1 * q / 2 + 1;
            }
        }
    }

    public void addXOffset(float value)
    {
        boolean increase = (value > 0);

        if (increase)
        {
            if (topLeftOfScreen.q > map.getWidth()[0] + 1)
            {
                screenOffsetX += value;

                checkOffsets();
            }
        } else
        {
            if (topLeftOfScreen.q + tilesOnScreenX < map.getWidth()[1] + 1)
            {
                screenOffsetX += value;

                checkOffsets();
            }
        }
    }

    public void addYOffset(float value)
    {
        boolean increase = (value > 0);

        if (increase)
        {
            if (topLeftOfScreen.r > map.getHeightArray(topLeftOfScreen)[1].r)
            {
                screenOffsetY += value;

                checkOffsets();
            }
        } else
        {
            HexPoint[] array = map.getHeightArray(topLeftOfScreen);
            if (topLeftOfScreen.r + tilesOnScreenY < array[array.length - 1].r)
            {
                screenOffsetY += value;

                checkOffsets();
            }
        }
    }

    public void checkOffsets()
    {
        if (screenOffsetX > 1)
        {
            topLeftOfScreen.q--;
            screenOffsetX--;
            screenOffsetY -= 0.5f;
        } else if (screenOffsetX < 0)
        {
            topLeftOfScreen.q++;
            screenOffsetX++;
            screenOffsetY += 0.5f;
        }

        if (screenOffsetY > 1)
        {
            topLeftOfScreen.r--;
            screenOffsetY--;
        } else if (screenOffsetY < 0)
        {
            topLeftOfScreen.r++;
            screenOffsetY++;
        }
    }
}
