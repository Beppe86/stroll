/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 HelpWorldView
 Sep 16, 2015
 */
package main.gui;

import java.awt.Point;
import main.map.Tile;
import main.system.Game;
import main.system.HexPoint;

/**
 *
 * @author Beppe
 */
public class HelpWorldView
{

    public static HexPoint calcClickToHex(int x, int y, HexPoint topLeftHex, float screenOffsetX, float screenOffsetY)
    {
        x -= screenOffsetX * Tile.width * 3 / 4;
        y -= screenOffsetY * Tile.height;

        int q = x * 2 / 3 / Game.scale;
        int r = (int) Math.round((-x / 3 + Math.sqrt(3) / 3 * y) / Game.scale);

        q += topLeftHex.q;
        r += topLeftHex.r;

        return new HexPoint(q, r);
    }

    public static Point calcCoordsToPixels(HexPoint p, float screenOffsetX, float screenOffsetY)
    {
        double horizGap = Tile.width * 3 / 4;
        double vertiGap = Tile.height;
        double offsetX = screenOffsetX * Tile.width * 3 / 4;
        double offsetY = screenOffsetY * Tile.height;
        p.setQ(p.q - 2);
        p.setR(p.r - 2);

        int x, y;

        if (p.q % 2 == 0)
        {
            x = (int) (p.q * horizGap + offsetX);
            y = (int) ((p.r * vertiGap) + (p.q / 2 * vertiGap) + offsetY);
            return new Point(x, y);
        } else
        {
            x = (int) (p.q * horizGap + offsetX);
            y = (int) ((p.r * vertiGap) + (vertiGap / 2) + ((p.q - 1) / 2 * vertiGap) + offsetY);
            return new Point(x, y);
        }

    }
}
