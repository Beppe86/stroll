/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 HelpMath
 Sep 25, 2015
 */
package main.system;

import javafx.geometry.Point3D;

/**
 *
 * @author Beppe
 */
public class HelpMath
{

    public Point3D axialToCube(HexPoint p)
    {
        int x = p.q;
        int z = p.r;
        int y = -x -z;
        return new Point3D(x, y, z);
    }
    
    public HexPoint cubeToAxial(Point3D p)
    {
        int q = (int)p.getX();
        int r = (int)p.getZ();
        return new HexPoint(q,r);
    }
}
