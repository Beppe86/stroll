/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 HexPoint
 Sep 16, 2015
 */
package main.system;

import java.awt.Point;

/**
 *  Replacement for point class to easier handle the coordinates in a hexogon map
 * q = column
 * r = tilted row from top left to bottom right
 * @author Beppe
 */
public class HexPoint extends Point implements java.io.Serializable
{
    public int q;
    public int r;
    
    public HexPoint(int q, int r)
    {
        super(q,r);
        this.q = q;
        this.r = r;
    }
    
    public double getQ()
    {
        q = super.x;
        return q;
    }
    
    public void setQ(int value)
    {
        super.x = value;
        q = super.x;
        
    }
    
    public int getR()
    {
        r = super.y;
        return r;
    }
    
    public void setR(int value)
    {
        super.y = value;
        r = super.y;
    }
}
