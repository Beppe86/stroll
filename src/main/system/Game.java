/*
 Andreas Erlandsson 860802
 beppe86@gmail.com

 Game
 Sep 21, 2015
 */
package main.system;

import main.gui.ClickSequences;
import main.gui.Screen;
import main.map.GeneratorMap;
import main.map.Map;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

/**
 *
 * @author Beppe
 */
public class Game extends BasicGame
{

    private Map map;
    private Screen screen;

    public static int scale = 50;

    public Game()
    {
        super("Stroll");
    }

    @Override
    public void init(GameContainer container) throws SlickException
    {
        GeneratorMap createdMap = new GeneratorMap(32);
        this.map = createdMap.getMap();
        screen = new Screen(container, map, new ClickSequences(map));
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException
    {
        if (container.getInput().isKeyDown(Input.KEY_W))
        {
            screen.addYOffset(0.01f);
        }
        if (container.getInput().isKeyDown(Input.KEY_S))
        {
            screen.addYOffset(-0.01f);
        }
        if (container.getInput().isKeyDown(Input.KEY_A))
        {
            screen.addXOffset(0.01f);
        }
        if (container.getInput().isKeyDown(Input.KEY_D))
        {
            screen.addXOffset(-0.01f);
        }
    }

    public void mouseClicked(int button, int x, int y, int clickCount)
    {
        screen.hexClicked(x, y);
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException
    {
        screen.render(g, map);
    }

}
